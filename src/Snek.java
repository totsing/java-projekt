import java.awt.Point;
import java.util.ArrayList;

/**
 * Created by Timo on 03.06.2017.
 *
 * Should contain almost everything that has to do with the snake.
 */
public class Snek {
    /** Creates a new Snek.*/
    public static final Snek S = new Snek();

    /** Sets the starting tail length.*/
    private final int baseTailLength = 3;

    /** Creates a new array list of points, this basically is the snake.*/
    private ArrayList<Point> snekParts = new ArrayList<>();

    /** The head part of the snake.*/
    private Point head;

    /** Starting X-cord of the snake. */
    private final int headStartX = 33;

    /** Starting Y-cord of the snake.*/
    private final int headStartY = 31;

    /**
     * Assigns snake's start point and length.
     */
    public Snek() {
        head = new Point(headStartX, headStartY);
        for (int i = 0; i < baseTailLength; i++) {
            snekParts.add(new Point(head.x, head.y));
        }
    }

    /**
     *
     * @return Returns snake's head(last point in "snekParts").
     */
    public final Point getHead() {
        return snekParts.get(snekParts.size() - 1);
    }

    /**
     *
     * @return Returns an ArrayList of snake parts.
     */
    public final ArrayList<Point> getSnekParts() {
        return snekParts;
    }

    /** Adds another snake part to array list with the cords of head. */
    public final void addSnekParts() {
        snekParts.add(new Point(head.x, head.y));
    }

    /** Removes the last snake part on each movement. */
    public final void remSnekparts() {
        snekParts.remove(0);
    }

    /** Sets the snake's direction to UP by subtracting from the Y-cord. */
    public final void dirUp() {
        head = new Point(head.x, head.y - 1);
    }

    /** Sets the snake's moving direction to DOWN by adding to the the Y-cord.*/
    public final void dirDown() {
        head = new Point(head.x, head.y + 1);
    }

    /** Sets the snake's direction to LEFT by subtracting from the X-cord.*/
    public final void dirLeft() {
        head = new Point(head.x - 1, head.y);
    }

    /** Sets the snake's moving direction to RIGHT by adding to the X-cord.*/
    public final void dirRight() {
        head = new Point(head.x + 1, head.y);
    }
    /**
     *
     * @return Returns if snake has collided with itself.
     */
    public final boolean snekCollison() {
        for (int i = 0; i < snekParts.size(); i++) {
            boolean xIntersect = head.x == snekParts.get(i).x;
            boolean yIntersect = head.y == snekParts.get(i).y;
            if (snekParts.size() > baseTailLength && xIntersect && yIntersect) {
                return true;
            }
        }
        return false;
    }
}
