import javax.sound.sampled.*;
import java.io.IOException;
import java.net.URL;

/**
 * Created by Timo on 07.06.2017.
 *
 * ----------------------------------------------------------------------------
 * | Game end sound is made by cabled_mess, and is public domain              |
 * |--------------------------------------------------------------------------|
 * |     Licence: https://creativecommons.org/publicdomain/zero/1.0/          |
 * |     Source: https://freesound.org/people/cabled_mess/sounds/350986/      |
 * ----------------------------------------------------------------------------
 *
 * ----------------------------------------------------------------------------
 * | Background music is made by neehnahw at free sound and is public domain. |
 * |--------------------------------------------------------------------------|
 * |     Licence: https://creativecommons.org/publicdomain/zero/1.0/          |
 * |     Source: https://freesound.org/people/neehnahw/sounds/332020/         |
 * ----------------------------------------------------------------------------
 *
 * ----------------------------------------------------------------------------
 * | Eating sound is cut from "Eat an apple" made by DjDust at free sound.    |
 * |--------------------------------------------------------------------------|
 * |    Licence: https://creativecommons.org/licenses/by/3.0/                 |
 * |    Source: https://freesound.org/people/DjDust/sounds/267531/            |
 * ----------------------------------------------------------------------------
 *
 */
public class Sounds {
    /**
     *
     */
    public static final Sounds SOUNDS = new Sounds();
    /** */
    private Clip eat;
    /** */
    private Clip bgm;
    /** */
    private Clip end;

    /**
     * Plays the background music on a loop.
     */
    public final void playBGM() {
        URL url = this.getClass().getClassLoader().getResource("Sounds/bgm.wav");
        AudioInputStream audioIn = null;
        try {
            audioIn = AudioSystem.getAudioInputStream(url);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            bgm = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        try {
            bgm.open(audioIn);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        bgm.loop(Clip.LOOP_CONTINUOUSLY);
    }

    /**
     * Plays the sound made when edibles are being eaten.
     */
    public final void playEatSound() {
        URL url = this.getClass().getClassLoader().getResource("Sounds/eat.wav");
        AudioInputStream audioIn = null;
        try {
            audioIn = AudioSystem.getAudioInputStream(url);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            eat = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        try {
            eat.open(audioIn);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        eat.loop(0);
    }

    /**
     * Plays the ending sound.
     */
    public final void playEnd() {
        URL url = this.getClass().getClassLoader().getResource("Sounds/end.wav");
        AudioInputStream audioIn = null;
        try {
            audioIn = AudioSystem.getAudioInputStream(url);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            end = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        try {
            end.open(audioIn);
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        end.loop(0);
    }

    /**
     * Stops the background music.
     */
    public final void stopBGM() {
        bgm.stop();
    }


}