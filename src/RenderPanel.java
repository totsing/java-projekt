import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;

/**
 * Created by Timo on 03.06.2017.
 *
 * This handles all of the drawing except the start dialog box.
 */
public class RenderPanel extends JPanel {
    /** Ending score positions. */
    private final int scoreX = 250, scoreY = 330;
    /** Game over sign positions. */
    private final int gOverX = 230, gOverY = 280;
    /** Font size. */
    private final int fontSize = 50;
    /** In game score positions. */
    private final int miniScoreX = 690, miniScoreY = 642;
    /** Position and thickness for the bottom border substitute. */
    private final int posOfBotBorder = 632, botBorderThickness = 12;
    /** Grass colour. */
    private Color grassGreen = new Color(112, 156, 52);
    /** Apple's colour. */
    private Color appleGreen = new Color(11, 112, 0);
    /** Snake's colour. */
    private Color snakeColor = new Color(4, 30, 56);
    /** Border's colour. */
    private Color borderColor = new Color(13, 20, 0);
    /** Cherry's colour. */
    private Color cherryRed = new Color(204, 25, 25);
    /** Game Over colour. */
    private Color gameOver = new Color(81, 0, 0);

    @Override
    public final void paint(final Graphics g) {
        super.paintComponent(g);
        // Imports the other files.
        Game game = Game.game;
        Edibles e = Edibles.E;
        Snek s = Snek.S;
        int scale = Game.SCALE;
        // Sets the background colour to dark gray and fills the window.
        g.setColor(grassGreen);
        g.fillRect(0, 0, Game.TABLE_WID, Game.TABLE_LEN);
        // Sets the border(except the bottom one).
        getRootPane().setBorder(BorderFactory.createMatteBorder(10, 10, 0, 12, borderColor));
        // Handles drawing the snake.
        g.setColor(snakeColor);
        for (Point point : s.getSnekParts()) {
            g.fillRect(point.x * scale, point.y * scale, scale, scale);
        }
        // Draws the red cherry.
        g.setColor(cherryRed);
        int cherryX = e.getCherryX();
        int cherryY = e.getCherryY();
        g.fillRect(cherryX * scale, cherryY * scale, scale, scale);
        // Draws the green apple.
        g.setColor(appleGreen);
        int appleX = e.getAppleX();
        int appleY = e.getAppleY();
        g.fillRect(appleX * scale, appleY * scale, scale, scale);
        // Makes the bottom "border" because
        // I couldn't write the score on the border.
        g.setColor(borderColor);
        g.fillRect(0, posOfBotBorder, Game.TABLE_WID, botBorderThickness);
        // If the state is GAME.
        if (game.getState() == Game.STATE.GAME) {
            // Shows the score on bottom right corner.
            g.setColor(Color.WHITE);
            g.drawString("Score: " + game.getScore(), miniScoreX, miniScoreY);
        } else if (game.getState() == Game.STATE.END) { // If the state is END.
            g.setColor(gameOver);
            // Draws the game over screen with score beneath it.
            Font f = new Font(Font.DIALOG, Font.BOLD, fontSize);
            setFont(f);
            g.drawString("GAME OVER", gOverX, gOverY);
            g.drawString("Score: " + game.getScore(), scoreX, scoreY);
        }

    }
}