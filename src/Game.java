import javax.swing.JFrame;
import javax.swing.Timer;
import javax.swing.WindowConstants;

//import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/***
 * Created by Timo on 07.01.2017.
 *
 * TODO: Difficulties(not enough time), location of the window(bruted), hi-score?
 *
 */
public class Game implements ActionListener, KeyListener {
    /** TODO: Fix for the running in main thing. */
    public static  Game game;

    /** Table size in pixels. */
    public static final int TABLE_WID = 800, TABLE_LEN = 700;

    /** The location of the window on the screen. */
    public static final int FRAME_LOC_X = 560, FRAME_LOC_Y = 190;

    /** Directions and the corresponding ints. */
    public static final int UP = 0, DOWN = 1, LEFT = 2, RIGHT = 3;

    /** The scale of the game, so every 10px is 1 square on the field. */
    public static final int SCALE = 10;

    /** Creates a JFrame called jframe. */
    private JFrame jframe;

    /** Creates a RenderPanel called renderPanel. */
    private RenderPanel renderPanel;

    /** Creates a Timer called timer. */
    private Timer timer = new Timer(10, this);

    /** After how many ticks the next movement should happen. */
    private final int gameSpeed = 4;

    /** Counts the ticks. */
    private int ticks = 0;

    /** Time in milliseconds to sleep(3000 = 3s). */
    private final int timeMs = 3000;

    /** The direction the snake is going, DOWN by default. */
    private int direction = RIGHT;

    /** Game score int. */
    private int score = 0;

    /** So the snake won't go out of game map(behind the border on X-axis). */
    private final int xOuterBorderFixer = 5;

    /** So the snake won't go out of game map(behind the border on Y-axis. */
    private final int yOuterBorderFixer = 8;

    /** So the snake won't turn on itself. */
    private boolean isNextMove = true;

    /**
     * Different states for tha game.
     */
    public enum STATE {
        /** */
        GAME,
        /** */
        MENU,
        /** */
        END
    }

    /** Couldn't figure out a way that makes checkstyle happy. */
    public static STATE state = STATE.MENU;

    /** Create Edibles edibles. */
    private Edibles edibles = Edibles.E;

    /** Assign name s to Snek.S. */
    private Snek s = Snek.S;
    /** Assign name sounds to Sounds.SOUNDS. */
    private Sounds sounds = Sounds.SOUNDS;

    /**
     *
     */
    public Game() {
        if (state == STATE.MENU) {
            Menu menu = new Menu();
        }
        sounds.playBGM();
        renderPanel = new RenderPanel();
        jframe = new JFrame("snek");
        jframe.setLocation(FRAME_LOC_X, FRAME_LOC_Y);
        jframe.add(renderPanel);
        jframe.setVisible(true);
        jframe.setSize(TABLE_WID, TABLE_LEN);
        jframe.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        if (state == STATE.GAME) {
            jframe.addKeyListener(this);
            timer.start();
        }
    }

    /**
     *
     * @param args Making the checkstyle happy.
     */
    public static void main(final String[] args) {
        game = new Game();
    }

    /**
     *
     * @param e Bla bla.
     */
    @Override
    public final void actionPerformed(final ActionEvent e) {
        renderPanel.repaint();
        if (state == STATE.GAME) {
            ticks++;
        }
        if (state == STATE.END) {
            timer.stop();
            sounds.stopBGM();
            sounds.playEnd();
            try {
                Thread.sleep(timeMs);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }

        if (ticks % gameSpeed == 0 && s.getHead() != null) {
            s.addSnekParts();
            s.remSnekparts();
            if (direction == UP) {
                s.dirUp();
                isNextMove = true;
            }
            if (direction == DOWN) {
                s.dirDown();
                isNextMove = true;
            }
            if (direction == LEFT) {
                s.dirLeft();
                isNextMove = true;
            }
            if (direction == RIGHT) {
                s.dirRight();
                isNextMove = true;
            }
            if (edibles.eatApple()) {
                sounds.playEatSound();
                score += Edibles.APPLE_VALUE;
                s.addSnekParts();
                edibles.setAppleHidden();
            } else if (edibles.eatCherry() || edibles.getCherry() == null) {
                sounds.playEatSound();
                score += Edibles.CHERRY_VALUE;
                s.addSnekParts();
                edibles.setCherry();
                if (edibles.isSpawnApple() && edibles.getAppleX() == -1) {
                    edibles.setApple();
                }
            } else if (s.snekCollison()) {
                state = STATE.END;
            }
            if (wallCollision()) {
                state = STATE.END;
            }
        }

    }

    /**
     *
     * @param e Useless key event.
     */
    @Override
    public void  keyTyped(final KeyEvent e) {

    }

    /**
     *
     * @param e Sets the direction to the direction of the pressed arrow key.
     */
    @Override
    public final void keyPressed(final KeyEvent e) {
        int i = e.getKeyCode();
        if (i == KeyEvent.VK_UP && direction != DOWN && isNextMove) {
            direction = UP;
            isNextMove = false;
        } else if (i == KeyEvent.VK_DOWN && direction != UP && isNextMove) {
            direction = DOWN;
            isNextMove = false;
        } else if (i == KeyEvent.VK_LEFT && direction != RIGHT && isNextMove) {
            direction = LEFT;
            isNextMove = false;
        } else if (i == KeyEvent.VK_RIGHT && direction != LEFT && isNextMove) {
            direction = RIGHT;
            isNextMove = false;
        }
    }

    /**
     *
     * @param e Useless key event.
     */
    @Override
    public void keyReleased(final KeyEvent e) {

    }

    /**
     *
     * @return Returns if snake has collided with the wall.
     */
    public final boolean wallCollision() {
        int x = s.getHead().x;
        int y = s.getHead().y;
        boolean outRight = x > (TABLE_WID / SCALE) - xOuterBorderFixer;
        boolean outLeft = y > (TABLE_LEN / SCALE) - yOuterBorderFixer;
        return y < 0 || x < 0 || outRight || y < 0 || outLeft;
    }

    /**
     *
     * @return Gets current score.
     */
    public final int getScore() {
        return score;
    }

    /**
     * Sets the state to GAME, doesn't work in Menu.java.
     */
    public final void setStateToGame() {
        state = STATE.GAME;
    }

    /**
     *
     * @return Returns the current state of the game.
     */
    public final STATE getState() {
        return state;
    }
}