import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * Created by Timo on 03.06.2017.
 *
 * The startup dialog window.
 *
 */
public class Menu {
    /** The location for the startup dialog box. */
    private final int boxX = 560, boxY = 190;

    /** */
    public Menu() {
        /* Makes a new JFrame. */
        JFrame jframe = new JFrame("Startup Dialog");
        jframe.setLocation(boxX, boxY);
        Object[] options = {"Start", "Exit"};
        int n = JOptionPane.showOptionDialog(jframe,
                "Would you like to start Snake?",
                "Snake",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[0]);
        if (n == 0) {
            Game.state = Game.STATE.GAME;
        } else if (n == 1) {
            System.exit(0);
        }
    }

}
