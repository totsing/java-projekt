import java.awt.Point;
import java.util.Random;

/**
 * Created by Timo on 05.06.2017.
 *
 * Almost anything that has to do with edibles and their settings is here.
 *
 */
public class Edibles {
    /** Create new Edibles E. */
    public static final Edibles E = new Edibles();

    /** Sets the value for eating a cherry. */
    public static final int CHERRY_VALUE = 1;

    /** Sets the value for eating an apple. */
    public static final int APPLE_VALUE = 20;

    /** Creates the point named cherry. */
    private Point cherry;

    /** Creates the point named apple. */
    private Point apple;

    /** So the edible won't go out of game map(behind the border on X-axis). */
    private final int xOuterBorderFixer = 5;

    /** So the edible won't go out of game map(behind the border on Y-axis. */
    private final int yOuterBorderFixer = 8;

    /** Sets out of how many numbers one is picked, right now 10 = 10%. */
    private final int appleChanceOutOf = 10;

    /** Create new random. */
    private Random random = new Random();

/** Sets Snek s as Snek.S. */
    private Snek s = Snek.S;

    /**
     * Sets default points for "cherry" and "apple".
     */
    public Edibles() {
        cherry = new Point(getNewEdibleX(), getNewEdibleY());
        apple = new Point(-1, -1);

    }

    /**
     *
     * @return Returns the point "cherry".
     */
    public final Point getCherry() {
        return cherry;
    }

    /**
     *
     * @return Returns the point "apple".
     */
    public final Point getApple() {
        return apple;
    }

    /**
     * Set a new location for cherry.
     */
    public final void setCherry() {
        cherry.setLocation(getNewEdibleX(), getNewEdibleY());
    }

    /**
     * Sets apple on the screen somewhere.
     */
    public final void setApple() {
        apple.setLocation(getNewEdibleX(), getNewEdibleY());
    }

    /**
     * Sets apple out of playing field so it would be hidden.
     */
    public final void setAppleHidden() {
        apple.setLocation(-1, -1);
    }

    /**
     *
     * @return Cherry's X - Coordinate.
     */
    public final int getCherryX() {
        return cherry.x;
    }

    /**
     *
     * @return Cherry's X - Coordinate.
     */
    public final int getCherryY() {
        return cherry.y;
    }

    /**
     *
     * @return Apple's X - Coordinate.
     */
    public final int getAppleX() {
        return apple.x;
    }

    /**
     *
     * @return Apple's Y - Coordinate.
     */
    public final int getAppleY() {
        return apple.y;
    }

    /**
     *
     * @return New edible's X coordinate.
     */
    public final int getNewEdibleX() {
        return random.nextInt(Game.TABLE_WID / Game.SCALE - xOuterBorderFixer);
    }

    /**
     *
     * @return New edible's Y coordinate.
     */
    public final int getNewEdibleY() {
        return random.nextInt(Game.TABLE_LEN / Game.SCALE - yOuterBorderFixer);
    }

    /**
     *
     * @return Returns if Apple should be made.
     */
    public final boolean isSpawnApple() {
        return random.nextInt(appleChanceOutOf) == 1;
    }

    /**
     *
     * @return Returns if cherry is eaten.
     */
    public final boolean eatCherry() {

        return  s.getHead().x == getCherryX() && s.getHead().y == getCherryY();
    }

    /**
     *
     * @return Returns if apple is eaten.
     */
    public final boolean eatApple() {

        return  s.getHead().x == apple.x && s.getHead().y == apple.y;
    }
}